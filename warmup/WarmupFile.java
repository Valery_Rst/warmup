package net.warmup;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Задача разминка. Требуется определить максимальную прибыль для компании по известным
 * времени и стоимости заказов
 *
 * Алгоритм: получить исходные данные, отсортировать массив заказов по возрастанию,
 * найти прибыль от самых выгодных (самых последних в массиве) заказов
 *
 * Задача усложнена тем, что в файле модет быть записан не один комплект данных, а несколько
 */

public class WarmupFile {
    private static final String RESULT_FORMAT = "За время %d компания сможет выполнить %d заказов. Максимальная прибыль составит %d%n";

    private static ArrayList<Integer[]> complects = new ArrayList<Integer[]>();
    private static int[][] results;

    public static void main(String[] args) {
        readData();
        results = new int[complects.size()][3];
        for(int i = 0; i < complects.size(); i++) {
            results[i] = calculateResult(complects.get(i));
        }
        writeResults();
    }

    /**
     * Возвращает результат своей работы для конкретного комплекта
     *
     * @param complect комплект данных
     * @return результат своей работы для конкретного комплекта
     */
    public static int[] calculateResult(Integer[] complect) {

        /**
         * Результат для каждого комплекта данных хранится в массиве, где:
         * первый элемент - время, доступное компании
         * второй - кол-во выполненных заказов
         * третий - максимальная прибыль
         */

        int[] result = new int[3];
        result[0] = complect[0];

        /**
         * На самом деле кол-во заказов, которые компания может выполнить зависит от двух величин
         * и равно наименьшей из них. Т. е. если компании доступно время time и count_orders заказов
         * то компания сможет выполнить Math.min(orders_count, time)
         *
         * Пример: time = 4, orders_count = 2
         * Времени у компании полно, однако заказов всего два.
         * Возможна и обратная ситуация, когда заказов больше, чем времени. Тогда нужно выполнить самые лакомые
         * заказы для получения максимального дохода
         *
         * В конкретном случае time хранится в комплекте данных как первый элемент (индекс 0), а кол-во заказов
         * равно длине массива, представляющего комплект - 1, т.к. первый его элемент - время
         */

        result[1] = Math.min(complect[0], complect.length - 1);

        /**
         * Чтобы время, доступное для компании не играло роли в сортировке массива и не пердставлялось как заказ, на его место встаёт
         * величина -2^31
         */

        complect[0] = Integer.MIN_VALUE;

        /**
         * Сортировка массива
         */
        Arrays.sort(complect);

        result[2] = 0;

        /**
         * Подсчёт прибыли путём сложения прибыли от самых выгодных заказов
         */

        for(int i = 1; i <= result[1]; i++) {
            result[2] += complect[complect.length - i];
        }

        return result;
    }

    /**
     * Читает комплекты исходных данных в список complects
     */

    public static void readData() {

        /**
         * Объект BufferedReader озволяет производить построчное чтение файла
         */

        try(BufferedReader bufferedReader = new BufferedReader(new FileReader("orders.txt"))) {
            String line;
            String[] parts;
            Integer[] complect;

            while((line = bufferedReader.readLine()) != null) {
                /**
                 * К уже прочитанной строке конкатенирируем следующую строчку
                 * Это необходимо для реализации парсинга данных прямо в теле цикла
                 */
                line += " " + bufferedReader.readLine();
                /**
                 * Исходные данные в формате STRING, полученные разделением комплекта данных через пробелы
                 */
                parts = line.split(" ");
                /**
                 * Исходные данные в формате INTEGER, полученные преобразованием строковых данных методом valueOf
                 */
                complect = new Integer[parts.length];

                for(int i = 0; i < parts.length; i++) {

                    complect[i] = Integer.valueOf(parts[i]);
                }
                /**
                 * Сохранение прочитанного комплекта данных в списке complects
                 */
                complects.add(complect);
            }
        } catch (IOException ioe) {
            System.out.println("Что-то пошло не так");
        }
    }

    public static void writeResults() {
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("results.txt"))) {
            /**
             * Каждый массив из трёх элементов в массиве results - резудьтат работы подпрограммы для конкретного комплекта данных
             * первый элемент - время, доступное компании
             * второй - кол-во выполненных заказов
             * третий - максимальная прибыль
             */
            for(int[] result : results) {
                bufferedWriter.write(String.format(RESULT_FORMAT, result[0], result[1], result[2]));
            }
        } catch (IOException ioe) {
            System.out.println("Что-то пошло не так");
        }
    }
}